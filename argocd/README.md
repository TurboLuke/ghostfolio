
(Login)

````
argocd login <ARGO_CD_HOST>
````


Backup
````
$ argocd app get ghostfolio -o yaml > ghostfolio-argocd-backup.yaml
````


Restore
````
$ argocd app create --file ghostfolio-argocd-backup.yaml
````